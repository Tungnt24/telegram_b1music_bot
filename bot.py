import re
import os
import time
import requests
import logging

import telebot
from telebot import types
from telebot.types import InlineKeyboardButton, InlineKeyboardMarkup, ReplyKeyboardMarkup
import pafy
from flask import Flask, request
from search import *
from file_setup import create_file, read_file, delete_chat_id_cache_files


bot_token = os.getenv('BOT_TOKEN')
channel_id = os.getenv('MUSIC_CHANNEL_ID')
send_music_api = os.getenv('SEND_MUSIC_API')

server = Flask(__name__)

logging.basicConfig(level=logging.DEBUG,
                    format=' %(asctime)s - %(levelname)s - %(message)s')

bot = telebot.TeleBot(bot_token)


@bot.message_handler(commands=['start'])
def send_welcome(message):
    bot.reply_to(message, 
    """
    Hello, I'm B1 bot from B1Corp
What song are you looking for?""")


@bot.message_handler(commands=['help'])
def send_help(message):
    bot.reply_to(message,
    '''Just send me a song name/or video url and I will find music for you!
    ⚠ Duration less than 50 minutes''')


def send_to_api(chat_id, input_data):
    try:
        download_and_send = send_music_api.format(chat_id, input_data)
        res = requests.get(download_and_send)
    except Exception as err:
        logging.error("cannot send to api: %s | video_id: %s", err, chat_id)


def check_duration(video_id):
    get_duraion = pafy.new(video_id).duration
    check = list(map(int, get_duraion.split(':')))
    if (check[0] / 60 == 0) and (check[1] / 60) < (50 / 60):
        return True
    else:
        return False


@bot.message_handler(func=lambda message: True)
def show_title(message):
    chat_id = message.from_user.id

    # Delete cache file of chat id
    delete_chat_id_cache_files(chat_id)

    search_string = message.text

    regex = re.compile('^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+')
    is_url = regex.match(search_string)
    if is_url:
        check = check_duration(search_string)
        if check:
            send_to_api(chat_id, search_string)
        else:
            bot.send_message(chat_id, text='⚠ Duration more than 50 minutes. Try /help')
    else:
        bot.send_message(chat_id, 'searching...')
        videos = search(search_string)
        titles = []
        for video in videos:
            title = video.get("title")
            index = video.get("index")
            titles.append(f"{index + 1}.{title}")
        str_titles = "\n".join(titles)
        
        num_of_videos = len(videos)
        if num_of_videos == 0:
            bot.send_message(chat_id, text="I found nothing!\n Try /help")
        else:
            markup = InlineKeyboardMarkup(row_width=5)
            buttons = []
            for index in range(num_of_videos):
                video: dict = videos[index]
                data = {
                    "video_id": video.get("id"), 
                    "chat_id": chat_id,
                    "title": video.get("title")
                }
                file_name = f"{chat_id}_{time.time()}"
                create_file(file_name, data)
                button = InlineKeyboardButton(text=f"{index + 1}",
                                              callback_data=file_name)
                buttons.append(button)
            markup.add(*buttons)
            bot.send_message(chat_id, str_titles, reply_markup=markup)


@bot.callback_query_handler(lambda query: query.data)
def button(query):
    chat_id = query.from_user.id
    data = read_file(query.data)
    if not data:
        return
    index = data.get("index", 0)
    title = data.get("title", "UNDEFINDED_TITLE")
    video_id = data.get('video_id')
  
    check = check_duration(video_id)
    if check:
        bot.send_message(chat_id, text=f'Downloading {title}')
        send_to_api(chat_id, video_id)
    else:
         bot.send_message(chat_id, text='⚠ Duration more than 50 minutes. Try /help')


@server.route('/' + bot_token, methods=['POST'])
def getMessage():
    bot.process_new_updates([telebot.types.Update.de_json(request.stream.read().decode('utf-8'))])
    return '!', 200


@server.route('/')
def webhook():
    bot.remove_webhook()
    bot.set_webhook(url=f'https://b1music-bot-develop.herokuapp.com/{bot_token}')
    return '!', 200


if __name__ == '__main__':
    server.run(host='0.0.0.0', port=int(os.environ.get('PORT', 5000)))
