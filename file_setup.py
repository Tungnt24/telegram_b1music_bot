import json
import glob
import os


def create_file(file_name, data):
    file_path = f"{file_name}.json"
    with open(file_path, "w") as file:
        json.dump(data, file)


def read_file(file_name):
    try:
        file_path = f"{file_name}.json"
        with open(file_path) as file:
            return json.load(file)
    except Exception:
        return None


def delete_chat_id_cache_files(chat_id):
    pattern = f"{chat_id}*"
    filelist = glob.glob(pattern)
    for filepath in filelist:
        try:
            os.remove(filepath)
        except:
            continue
