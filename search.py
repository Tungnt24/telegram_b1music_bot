from youtube_search import YoutubeSearch


def search(text):
    result = []
    search_result = YoutubeSearch(f"'{text}''", max_results=10).to_dict()
    counter = 0
    for video in search_result:
        data = {
            "title": video.get("title", "INVALID TITLE"),
            "id": video.get("id"),
            "index": counter
        }
        result.append(data)
        counter = counter + 1
    return result
